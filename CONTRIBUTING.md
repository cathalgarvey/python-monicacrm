============
Contributing
============

Contributions are welcome, and they are greatly appreciated! Every
little bit helps, and credit will always be given.

Please be aware that by contributing to this project you license
your contribution under the project's license (see `LICENSE`), and
you agree that your contribution may be used under the terms of
that license. You do not need to sign over ownership or rights or
titles of your contribution: the terms of the license are sufficient.

Bug reports
===========

When [reporting a bug](https://gitlab.com/cathalgarvey/python-monicacrm/issues) please include:

    * Your operating system name and version.
    * Any details about your local setup that might be helpful in troubleshooting.
    * Detailed steps to reproduce the bug.

Documentation improvements
==========================

monicacrm could always use more documentation, whether as part of the
official monicacrm docs, in docstrings, or even on the web in blog posts,
articles, and such.

Feature requests and feedback
=============================

The best way to send feedback is to file an issue at https://gitlab.com/cathalgarvey/python-monicacrm/issues .

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that code contributions are welcome :)

Please be aware that I may refuse a feature or addition on the grounds that it
would introduce an untenable maintenance burden. For example, a feature that
requires proprietary software to work, test, or develop, will not be included
in this repository.

Development
===========

To set up `python-monicacrm` for local development:

1. Fork [python-monicacrm](https://gitlab.com/cathalgarvey/python-monicacrm)
   (look for the "Fork" button).
2. Clone your fork locally:

    git clone git@gitlab.com:cathalgarvey/python-monicacrm.git

3. Create a branch for local development:

    git checkout -b name-of-your-bugfix-or-feature

   Now you can make your changes locally.

4. When you're done making changes, run all the checks, doc builder and spell checker with [tox](https://tox.readthedocs.io/en/latest/install.html) one command:

    tox

5. Commit your changes and push your branch to GitHub:

    git add .
    git commit -m "Your detailed description of your changes."
    git push origin name-of-your-bugfix-or-feature

6. Submit a pull request through the GitHub website.

Pull Request Guidelines
-----------------------

If you need some code review or feedback while you're developing the code just make the pull request.

For merging, you should:

1. Include passing tests (run `tox`).
2. Update documentation when there's new API, functionality etc.
3. Add a note to `CHANGELOG.md` about the changes.
4. Add yourself to `AUTHORS.md`.


Tips
----

To run a subset of tests:

    tox -e envname -- pytest -k test_myfeature

To run all the test environments in *parallel* (you need to `pip install detox`)::

    detox
