"""
CLI App for MonicaCRM. Intended primarily for bulk imports of CSV records.

Not yet, uh, anything
"""
import click
import pathlib
import logging

from . import MonicaAPI
from . import utils

# NB: click.progressbar

@click.group()
def main():
    pass


@main.command()
def login():
    config_folder = utils.MonicaConfigFolder('monicacrm')
    print("If you have not already, go here to create an API token: https://app.monicahq.com/settings/api")
    if not pathlib.Path(config_folder.app_folder).exists():
        click.confirm(f"Config folder at {app_folder()} is required. Create config folder for MonicaCRM?", abort=True)
        pathlib.Path(config_folder.app_folder).mkdir(mode=0o740,  # TODO: is this sane
                                                     parents=True,
                                                     exist_ok=True)
    token = input("Paste your access token and press enter: ").strip()
    config_folder.set_access_token(token)


@main.command(short_help="Print CSV template to terminal, direct to a file and open in a spreadsheet to begin adding contacts for bulk import.")
def csvtemplate():
    print("first_name,last_name,nickname,gender,tags,birthdate,Task #1,Task #1 description,Task #2,Task #2 description")


@main.command(short_help="Import contacts from a CSV with special headers.", help="""
A CSV file consisting of a header row and rows of contacts to import. Headers are important: only recognised headers will be used, and some are mandatory.

Mandatory: first_name; gender_name*

Optional: last_name; nickname; tags (comma-separated list); Task #1; Task #1 Description; Task #2; Task #2 Description.

*Gender is a Monica requirement for contacts. It must be a valid gender for your account: you can customise these. Unknown genders will trigger an informative error. I suggest adding 'Nonbinary' and 'Unknown' to your configs, as a minimum. Default genders are {Man, Woman, Rather Not Say}. This wrapper is case-insensitive to gender names.
""")
@click.argument("csvfile", type=click.Path(exists=True))
@click.option("--dry-run",
              is_flag=True,
              default=False,
              help=("Don't issue actual API queries, just log."
                    " You may need to configure loglevel to get"
                    " useful output if this is for debugging purposes."))
@click.option("--prefixed-tag-col",
              multiple=True,
              help=("In addition to an explicit 'tags' column, you can"
                    " also have additional sets of tags that will all"
                    " be prefixed with the column header if you name"
                    " the column here. e.g. a column 'profession', and"
                    " a row 'clerk,parent' would add tags"
                    " 'profession: clerk' and 'profession: parent'."
                    " This argument may be used multiple times."))
@click.option("--log-level",
              default='info',
              type=click.Choice(['critical',
                                 'error',
                                 'warning',
                                 'info',
                                 'debug']))
def csvimport(csvfile,
              dry_run,
              prefixed_tag_col,
              log_level,
              ):
    logging.basicConfig(level=log_level.upper())
    config_folder = utils.MonicaConfigFolder('monicacrm')
    if dry_run:
        logging.info("Dry Run mode enabled: No API requests will be made.")
        api = utils.DryRunAPI()
    else:
        api = MonicaAPI(config_folder.get_access_token())

    csv_records = utils.load_validated_from_csv(csvfile)
    # TODO: Move the below into a different function to allow multi-pass
    # uploads, for example to defer records requiring approval until
    # last.
    with click.progressbar(csv_records) as progress_records:
        for record in progress_records:
            # Prep
            logging.debug(f"Parsing CSV record for API arguments: {record}")
            new_contact = utils.populate_contact_args(api, record)
            logging.debug(f"\tnew_contact={new_contact}")
            contact_name = f"{new_contact['first_name']} {new_contact.get('last_name', '')}".strip()
            for column in prefixed_tag_col:
                assert column in record
            new_contact_tags = utils.populate_tags_args(record,
                                                        prefixed_tag_columns=prefixed_tag_col)
            logging.debug(f"\tnew_contact_tags={new_contact_tags}")
            new_contact_tasks = utils.populate_tasks_args(record)
            logging.debug(f"\tnew_contact_tasks={new_contact_tasks}")
    
            # Check
            if api.synonymous_contacts(new_contact['first_name'],
                                       new_contact['last_name']):
                if not click.confirm(f"\nDuplicate records found for name '{contact_name}', should a new record be made anyway?", default=False):
                    continue

            # Do
            logging.info(f"Creating new contact entry for {contact_name}..")
            resp = api.add_contact(**new_contact)
            contact_id = resp['data']['id']
            logging.debug(f"contact_id={contact_id}")
            if new_contact_tags:
                logging.info(f"Tagging {contact_name}..")
                api.tag_contact(contact_id, new_contact_tags)
            if new_contact_tasks:
                logging.info(f"Adding tasks for {contact_name}..")
                for task_args in new_contact_tasks:
                    logging.debug(f"\tAdding task: {task_args['title']}")
                    api.task_add(contact_id, **task_args)


if __name__ == "__main__":
    main()
