"""
Utility functions, mostly for the CLI utility.
"""

import re
import logging
import os
import os.path
import csv

import click


# === CSV and Import Utils ===

def _parse_birthday_mmdd(birthday)->(None, int, int):
    'For when year is not known..'
    try:
        month, day = re.match(r'(\d\d)-(\d\d)', birthday).groups()
        return None, int(month), int(day)
    except AttributeError:
        logging.error(f"Unparseable date: {birthday}")
        return (None, None, None)

def parse_birthday(birthdate, attempt_mmdd=True)->(int, int, int):
    if not birthdate: return (None, None, None)
    try:
        yearall, year_is_abs, month, day = re.match(
            r'(\d\d(\d\d)?)-(\d\d?)-(\d\d?)',
            birthdate).groups()
    except AttributeError:  # None returned
        if attempt_mmdd:
            return _parse_birthday_mmdd(birthdate)
        logging.error(f"Unparseable date: {birthdate} - Currently only {{year}}-{{month}}-{{day}} is supported, with full years. e.g. 1985-07-18")
        return (None, None, None)
    if not year_is_abs:
        logging.error(f"Unsupported date format: {birthdate}  - only four-digit years are supported in {{year}}-{{month}}-{{day}} format.")
        return (None, None, None)
    return tuple(map(int, (yearall, month, day)))


def load_validated_from_csv(filename,
                            require_keys=['first_name', 'gender']
                            ):
    required = set(require_keys)
    with open(filename, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if not required.intersection(row) == required:
                raise KeyError(f'Row lacks the required keys first_name and gender_name: {row}')
            yield row


def populate_tasks_args(csv_dict):
    tasks = {}
    for k,v in csv_dict.items():
        if not v.strip(): continue
        k = k.strip()
        if re.match(r'^[Tt]ask #(\d+)$', k):
            task_n = int(re.match(r'^[Tt]ask #(\d+)$', k).group(1))
            T = tasks.setdefault(task_n, {})
            T['title'] = v
            T.setdefault('index', task_n)
        if re.match(r'^[Tt]ask #\d+ [Dd]escription$', k):
            task_n = int(re.match(r'^[Tt]ask #(\d+) [Dd]escription$', k).group(1))
            T = tasks.setdefault(task_n, {})
            T['description'] = v
            T.setdefault('index', task_n)
    task_ds = sorted(tasks.values(), key=lambda T:T['index'])
    output_ds = []
    for T in task_ds: T.pop('index', None)
    for T in task_ds:
        if not ('title' in T): continue
        T.pop('index', None)
        output_ds.append(T)
    return output_ds


def populate_contact_args(monicaapi, csv_dict):
    bd_y, bd_m, bd_d = parse_birthday(csv_dict.get('birthdate'))
    args = dict(
        first_name=csv_dict['first_name'],
        last_name=csv_dict.get('last_name'),
        nickname=csv_dict.get('nickname'),
        gender=csv_dict['gender'],
        birthdate_day=bd_d,
        birthdate_month=bd_m,
        birthdate_year=bd_y,
    )
    args = {k:v for k,v in args.items() if v is not None}
    logging.debug(f"Looking for gender name {args['gender']} in all known genders: {monicaapi.all_genders}")
    return args


def _split_tags_list(tags_string, prefix_each=''):
    if not tags_string.strip(): return []
    tags_string = tags_string.lower().strip()
    tags_list = list(set(map(str.strip, tags_string.split(','))))
    if prefix_each:
        prefix_each = prefix_each.strip(': ').strip() + ": "
    return [f'{prefix_each}{tag}' for tag in tags_list]


def populate_tags_args(csv_dict, prefixed_tag_columns=[]):
    tags = _split_tags_list(csv_dict.get('tags', ''))
    for column_title in prefixed_tag_columns:
        prefixed_tags = _split_tags_list(
            csv_dict.get(column_title, ''),
            prefix_each=column_title)
        tags.extend(prefixed_tags)
    return list(set(tags))


# === Debug or Dry-Run ===

def dummy_method(expect):
    def _meth_maker(func):
        return lambda self, *a, **k: expect
    return _meth_maker


class _YesDict:
    def __init__(self, contents, default):
        self.contents = contents
        self.default = default

    def __getitem__(self, key):
        return self.contents.get(key, self.default)

    
class DryRunAPI:
    @dummy_method([])
    def search_contacts(): pass

    @dummy_method([])
    def search_contacts_all(): pass

    @dummy_method([])
    def synonymous_contacts(): pass

    @dummy_method(1)
    def resolve_contact_id(): pass

    @dummy_method({'data': {'id': 0}})
    def add_contact(): pass

    @dummy_method(None)
    def tag_contact(): pass

    @dummy_method(None)
    def untag_contact(): pass

    @dummy_method(None)
    def task_add(): pass

    @property
    def all_genders(self):
        return _YesDict({}, '')

    @property
    def all_tags(self):
        return _YesDict({}, '')


# === Home / Config folder helpers ===

class AppConfigFolder:
    "Click helper."

    def __init__(self, dir_name):
        self.dir_name = dir_name

    @property
    def app_folder(self):
        return click.get_app_dir(self.dir_name) 

    def _root_in_folder(self, filename):
        return os.path.join(self.app_folder, filename)

    def fetch(self, filename:str):
        with open(self._root_in_folder(filename), 'rb') as I:
            return I.read()

    def store(self, filename:str, contents:bytes, mode:int=None):
        filename = self._root_in_folder(filename)
        if mode is not None:
            with open(filename, 'a') as _: pass  # touch
            os.chmod(filename, mode)
        with open(filename, 'wb') as O:
            I.write(contents)


class MonicaConfigFolder(AppConfigFolder):
    def set_access_token(self, token:str):
        self.store('access_token', token.encode(), mode=0o640)

    def get_access_token(self):
        return self.fetch('access_token').decode()
