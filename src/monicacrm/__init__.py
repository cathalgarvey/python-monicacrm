__version__ = '0.1.0'

import requests
import re


class MonicaException(Exception): pass
class MonicaSearchFailure(MonicaException): pass
class MonicaAmbiguousQuery(MonicaException): pass


def _nullable_numeric_bool(boolornone: (bool,int,None)):
    "Monica allows nullable boolean flags but expects {1,0,None}, not {True,False,None}."
    if boolornone is None: return None
    else: return int(boolornone)

def _strip_nones(d):
    return {k:v for k,v in d.items() if v is not None}


class _MonicaAPIBase:
    def __init__(self,
                 access_token,
                 api_base='https://app.monicahq.com/api'):
        self._token = access_token
        self._api_base = api_base.rstrip('/')

    def _call_api(self, endpoint, method, params=None, request_data=None):
        method = method.lower()
        assert method in ('get', 'post', 'put', 'delete')
        r = getattr(requests, method)(
                self._api_base + '/' + endpoint.lstrip('/'),
                params=params,
                json=request_data,
                headers={
                    'Authorization': 'Bearer '+self._token,
                    'User-Agent': 'python-monicacrm (https://gitlab.com/cathalgarvey/python-monicacrm)',
                },
            )
        r.raise_for_status()
        return r.json()


class _MonicaAPIContacts(_MonicaAPIBase):
    def search_contacts(self, query, page=1, limit=25):
        next_page = lambda: self.search_contacts(query, page=page+1, limit=limit)
        resp = self._call_api('/contacts', 'get', params={'query': query})
        is_last = resp['meta']['current_page'] == resp['meta']['last_page']
        return resp['data'], None if is_last else next_page

    def search_contacts_all(self, query, per_page=100):
        results, page = self.search_contacts(query, limit=per_page)
        yield from results
        yield from (page or (lambda: []))()

    def synonymous_contacts(self, first_name, last_name):
        candidates = self.search_contacts_all(f'{first_name} {last_name}')
        matches = []
        for candidate in candidates:
            if (candidate['first_name'] == first_name and
                candidate['last_name'] == last_name):
                matches.append(candidate)
        return matches
    
    def add_contact(self,
                    first_name,
                    last_name=None,
                    nickname=None,
                    gender=None,
                    birthdate_day=None,
                    birthdate_month=None,
                    birthdate_year=None,
                    is_deceased=False,
                    is_deceased_date_known=False,
                    ):
        """
        Add a contact. Most of the fields are self-explanatory. Except:

        gender: This should be a plaintext version of a gender known to the API. This will be checked against a locally-held cache: genders will not be created on-the-fly when an unknown gender is encountered.
        tags: Not Implemented Yet
        """
        gender_id = self.all_genders[gender]
        is_birthdate_known=bool(birthdate_day
                                and birthdate_month
                                and birthdate_year)
        contact_data = dict(
                    first_name=first_name,
                    last_name=last_name,
                    nickname=nickname,
                    gender=gender,
                    birthdate_day=birthdate_day,
                    birthdate_month=birthdate_month,
                    birthdate_year=birthdate_year,
                    is_birthdate_known=is_birthdate_known,
                    is_deceased=is_deceased,
                    is_deceased_date_known=is_deceased_date_known,
        )
        return self._call_api('/contacts/', 'post', request_data=contact_data)

    def resolve_contact_id(self, contact_id_or_query, assert_unique=False):
        """
        Returns the query if it is an int.
        If it is a string, it runs a search and returns the _first result_.
        """
        assert isinstance(contact_id_or_query, (str, int))
        if isinstance(contact_id_or_query, int):
            return contact_id_or_query
        results, _ = self.search_contacts(contact_id_or_query, limit=2)
        if not results:
            raise MonicaSearchFailure(contact_id_or_query)
        if len(results) > 1:
            raise MonicaAmbiguousQuery(contact_id_or_query)
        first = results[0]
        return first['id']



        
class _MonicaAPIGifts(_MonicaAPIContacts):

    def format_gift(self,
                 gift_name:str,
                 gift_comment:str,
                 *,
                 gift_url:str=None,
                 gift_value:str=None,
                 is_an_idea:bool=None,
                 has_been_offered:bool=None,
                 date_offered:str=None,  # YYYY-MM-DD
                 is_for:int=None,                    
                 ):
        if date_offered is not None:
            assert re.match(r'\d\d\d\d-\d\d-\d\d', date_offered)
        gift_data = dict(
            # Mandatory
            name=gift_name,
            comment=gift_comment,
            # Optional
            url=gift_url,
            value=gift_value,
            is_an_idea=_nullable_numeric_bool(is_an_idea),
            has_been_offered=_nullable_numeric_bool(has_been_offered),
            date_offered=date_offered,
            is_for=is_for, #contact_id of secondary contact gift is for
        )
        return _strip_nones(gift_data)
        
    def add_gift(self,
                 contact_id_or_query:(str,int),
                 gift_name:str,
                 gift_comment:str,
                 *,
                 gift_url:str=None,
                 gift_value:str=None,
                 is_an_idea:bool=None,
                 has_been_offered:bool=None,
                 date_offered:str=None,  # YYYY-MM-DD
                 is_for:int=None,
                 ):
        """
        Add a new gift for a contact.
        """
        contact_id = self.resolve_contact_id(contact_id_or_query)
        gift_data = self.format_gift(
                 gift_name, gift_comment,
                 gift_url=gift_url,
                 gift_value=gift_value,
                 is_an_idea=is_an_idea,
                 has_been_offered=has_been_offered,
                 date_offered=date_offered,
                 is_for=is_for,
        )
        gift_data['contact_id'] = contact_id
        print(f'gift_data={gift_data}')
        resp = self._call_api('/gifts/', 'post', request_data=gift_data)
        return resp['data']

    def update_gift(self,
                 gift_id:int,
                 gift_name:str,
                 gift_comment:str,
                 *,
                 gift_url:str=None,
                 gift_value:str=None,
                 is_an_idea:bool=None,
                 has_been_offered:bool=None,
                 date_offered:str=None,  # YYYY-MM-DD
                 is_for:int=None,
                 ):
        """
        Update a gift.
        """
        gift_data = self.format_gift(
                 gift_name, gift_comment,
                 gift_url=gift_url,
                 gift_value=gift_value,
                 is_an_idea=is_an_idea,
                 has_been_offered=has_been_offered,
                 date_offered=date_offered,
                 is_for=is_for,
        )
        resp = self._call_api(f'/gifts/{gift_id}', 'put', request_data=gift_data)
        return resp['data']

    def delete_gift(self, gift_id):
        resp = self._call_api(f'/gifts/{gift_id}', 'delete')
        return resp['data']

    def iter_gifts(self, page=1, per_page_limit=25):
        """
        Fully paginate and yield all gifts for the account's contacts.
        """
        resp = self._call_api('/gifts/',
                              'get',
                              params=dict(page=page, limit=per_page_limit))
        yield from resp['data']
        is_last = resp['meta']['current_page'] == resp['meta']['last_page']
        if not is_last:
            yield from self.iter_gifts(page=page+1,
                                       per_page_limit=per_page_limit)

    def list_gifts(self, contact_id_or_query=None):
        """
        Either return all gifts for a particular contact,
        or all gifts for all contacts.
        """
        if contact_id_or_query is not None:
            contact_id = self.resolve_contact_id(contact_id_or_query)
            resp = self._call_api(f'/contacts/{contact_id}/gifts', 'get')
            return resp['data']
        else:
            return list(self.iter_gifts())


class _MonicaAPIReminders(_MonicaAPIContacts):
    """
    Handle getting,setting,editing,deleting reminders, both one-time and recurring.
    """
    def iter_reminders(self, page=1, per_page_limit=25):
        resp = self._call_api('/reminders/',
                              'get',
                              params=dict(page=page, limit=per_page_limit))
        yield from resp['data']
        is_last = resp['meta']['current_page'] == resp['meta']['last_page']
        if not is_last:
            yield from self.iter_reminders(page=page+1,
                                           per_page_limit=per_page_limit)

    def list_reminders(self, contact_id_or_query=None):
        if contact_id_or_query is not None:
            contact_id = self.resolve_contact_id(contact_id_or_query)
            resp = self._call_api(f'/contacts/{contact_id}/reminders', 'get')
            return resp['data']
        else:
            return list(self.iter_reminders())

    def format_reminder(self,
                        title:str,
                        next_expected_date:str,  # YYYY-MM-DD
                        frequency_type:{'one_time', 'week', 'month', 'year'},
                        frequency_number:int=None,
                        description:str=None,
                        ):
        assert re.match(r'\d\d\d\d-\d\d-\d\d', next_expected_date)
        assert frequency_type in {'one_time', 'week', 'month', 'year'}
        assert title and isinstance(title, str)
        return _strip_nones(dict(
            title=title,
            next_expected_date=next_expected_date,
            frequency_type=frequency_type,
            frequency_number=_nullable_numeric_bool(frequency_number),
            description=description,
        ))

    def create_reminder(self,
                        contact_id_or_query,
                        title:str,
                        next_expected_date:str,  # YYYY-MM-DD
                        frequency_type:{'one_time', 'week', 'month', 'year'},
                        frequency_number:int=None,
                        description:str=None,
                        ):
        contact_id = self.resolve_contact_id(contact_id_or_query)
        reminder_data = self.format_reminder(
            title,
            next_expected_date,
            frequency_type,
            frequency_number,
            description)
        resp = self._call_api('/reminders/', 'post', request_data=reminder_data)
        return resp['data']

    def update_reminder(self,
                        reminder_id,
                        title:str,
                        next_expected_date:str,  # YYYY-MM-DD
                        frequency_type:{'one_time', 'week', 'month', 'year'},
                        frequency_number:int=None,
                        description:str=None,
                        ):
        reminder_data = self.format_reminder(
            title,
            next_expected_date,
            frequency_type,
            frequency_number,
            description)
        resp = self._call_api(f'/reminders/{reminder_id}', 'put', request_data=reminder_data)
        return resp['data']

    def delete_reminder(self, reminder_id):
        resp = self._call_api(f'/reminders/{reminder_id}', 'delete')
        return resp['data']


class _MonicaContactFields(_MonicaAPIContacts):
    # TODO: Methods missing here for updates and deletion.

    def get_fields_list(self, contact_id_or_query):
        contact_id = self.resolve_contact_id(contact_id_or_query)
        resp = self._call_api(f'/contacts/{contact_id}/contactfields', 'get')
        return [
            dict(name=f['contact_field_type']['name'],
                 protocol=f['contact_field_type']['protocol'],
                 content=f['content'],
                 updated_at=f['updated_at'],
                 fa_css_classes=f['contact_field_type']['fontawesome_icon']
                )
            for f in resp['data']]

    def get_fields(self, contact_id_or_query):
        fields_l = self.resolve_contact_id(contact_id_or_query)
        m = {}
        for f in fields_l:
            m.setdefault(f['name'], []).append(f)
        return m

    def add_field(self, contact_id_or_query, field_name, field_value):
        contact_id = self.resolve_contact_id(contact_id_or_query)
        field_id = self.get_field_types()[field_name.lower()]
        data = dict(data=field_value,
                    contact_id=contact_id,
                    contact_field_type_id=field_id,
        )
        resp = self._call_api('/contactfields/', 'post', request_data=data)
        return resp['data']

    def _get_field_types(self, page=1, limit=100):
        fts = self._call_api('contactfieldtypes',
                             'get',
                             params=dict(limit=limit,
                                         page=page
                             ))
        field_types = fts['data']
        if fts['meta']['current_page'] < fts['meta']['last_page']:
            field_types.extend(self.get_field_types(page+1, limit))
        keyed_by_name = {}
        for field in field_types:
            # keyed_by_name[field['name']] = field['id']
            keyed_by_name[field['name'].lower()] = field['id']
        return keyed_by_name

    def get_field_types(self):
        if not hasattr(self, '_cached_field_types'):
            self._cached_field_types = self._get_field_types()
        return self._cached_field_types


class _MonicaAPITags(_MonicaAPIContacts):
    # Monica's Tag API does not have a clear way to search by tag :/

    def _all_tags(self):
        resp = self._call_api('/tags', 'get')
        return {i['name'].lower(): i['id'] for i in resp['data']}

    def _refresh_tag_cache(self):
        self._cached_all_tags = self._all_tags()
        
    @property
    def all_tags(self):
        if not hasattr(self, '_cached_all_tags'):
            self._refresh_tag_cache()
        return self._cached_all_tags

    def _refresh_cache_if_unseen(self, tags_list):
        """
        If an unknown tag is encountered, this will prompt a cache refresh
        from the API. It takes no further action, so other methods should
        still account for the possibility that a tag does not exist.
        """
        if set(map(str.lower, tags_list)).difference(self.all_tags):
            self._refresh_tag_cache()

    def resolve_tags(self, tags_list):
        self._refresh_cache_if_unseen(tags_list)
        ids = []
        for t in map(str.lower, tags_list):
            if not t in self.all_tags:
                raise KeyError(f'Unknown Tag: {t}')
            ids.append(self.all_tags[t])
        return ids

    def tag_contact(self, contact_id_or_query, tags_list):
        contact_id = self.resolve_contact_id(contact_id_or_query)
        data = { 'tags': tags_list }
        endpoint = '/contacts/{}/setTags'.format(contact_id)
        resp = self._call_api(endpoint, 'post', request_data=data)
        if not resp['data'].get('id', None) == contact_id:
            raise MonicaException(
                f'Contact ID returned by API was not the target contact ID: {contact_id} requested, {resp["data"].get("id")} returned')

    def untag_contact(self, contact_id_or_query, tags_list):
        contact_id = self.resolve_contact_id(contact_id_or_query)
        tag_ids = self.resolve_tags(tags_list)  # TODO
        data = { 'tags': tag_ids }
        endpoint = '/contacts/{}/unsetTag'.format(contact_id)
        resp = self._call_api(endpoint, 'post', request_data=data)
        if not resp['data'].get('id', None) == contact_id:
            raise MonicaException('Contact ID returned by API was not the target contact ID: {} requested, {} returned'.format(contact_id, resp['data'].get('id')))


class _MonicaAPIGenders(_MonicaAPIBase):

    def _all_genders(self):
        resp = self._call_api('/genders', 'get')
        gdrs = {i['name']: i['id'] for i in resp['data']}
        gdrs.update({k.lower():v for k,v in gdrs.items()})
        return gdrs

    @property
    def all_genders(self):
        if not hasattr(self, '_cached_all_genders'):
            self._cached_all_genders = self._all_genders()
        return self._cached_all_genders


class _MonicaAPICacheableFields(_MonicaAPITags,
                                 _MonicaAPIGenders):
    def _precache_fields(self):
        """
        Precache customisable, refer-by-id fields like Gender and Tags.
        This permits things like knowing when it is necessary to make
        a new tag, or when to prompt that a gender is unknown.
        """
        self.all_genders
        self.all_tags


class MonicaAPI(_MonicaAPICacheableFields,
                _MonicaAPIGifts,
                _MonicaAPIReminders,
                _MonicaContactFields,
                ):

    def task_add(self,
                 contact_id_or_query,
                 title:str,
                 *,
                 description:str=None,
                 completed:bool=False,
                 completed_at:str=None,
                 ):
        assert(isinstance(title, str) and len(title) <= 255)
        assert(isinstance(completed, bool))
        if description is not None:
            assert(isinstance(description, str) and len(title) <= 1000000)
        if completed_at is not None:
            assert(isinstance(completed_at, str) and
                   re.match(r'\d\d\d\d-\d\d-\d\d', completed_at))
        contact_id = self.resolve_contact_id(contact_id_or_query)
        data = {
            'title': title,
            'completed': int(completed),
            'contact_id': contact_id,
        }
        if description:
            data['description'] = description
        if completed_at:
            data['completed_at'] = completed_at
        resp = self._call_api('/tasks/', 'post', request_data=data)
        return resp['data']

    def add_note(self, contact_id_or_query, note_body, favourite=False):
        contact_id = self.resolve_contact_id(contact_id_or_query)
        resp = self._call_api('/notes/', 'post', request_data={
            "contact_id": contact_id,
            "body": note_body,
            "is_favorited": int(favourite),
        })
        return resp["data"]

    @property
    def countries(self):
        return self._call_api('/countries/', 'get')['data']


        next_page = lambda: self.search_contacts(query, page=page+1, limit=limit)
        resp = self._call_api('/contacts', 'get', params={'query': query})
        is_last = resp['meta']['current_page'] == resp['meta']['last_page']
        return resp['data'], None if is_last else next_page
